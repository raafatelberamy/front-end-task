// Select color input
// Select size input

// When size is submitted by the user, call makeGrid()
var submitButton=document.getElementById('submitButton'); 
  submitButton.onclick=function(){
  event.preventDefault() //prevent it from submitting a form
  var rows=document.getElementById('inputHeight').value; //get value of inputHeight from user
  var cols=document.getElementById('inputWidth').value;  //get value of inputWidthfrom user
  makeGrid(rows,cols); //calling function makeGrid
}
// this function used for drawing a canvas by input values of user
function makeGrid(rows,cols) {
  // Your code goes here!
  var table=document.getElementById('pixelCanvas');
   var tableTemp=""; 
  for(var i=0; i<rows; i++)
  {
    tableTemp+='<tr>'
     for(var j=0; j<cols; j++)
     {
       tableTemp+='<td></td>'
    }
     tableTemp+='</tr>'
   }
   table.innerHTML=tableTemp;
   _N_by_M_();//calling function  _N_by_M_
  }

  //this function used to select clickable td and set it's color by user
  function _N_by_M_() 
  {
    var tds=document.getElementsByTagName('td');
    for(var x=0; x<tds.length; x++){
     tds[x].addEventListener('click',function(e){
      var colorPicker=document.getElementById('colorPicker').value;
      var currentTd=e.target;
      currentTd.style.backgroundColor=colorPicker;
   })}
  }



